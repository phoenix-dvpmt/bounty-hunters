package net.Indyuce.bountyhunters.api.player;

import net.Indyuce.bountyhunters.BountyHunters;
import net.Indyuce.bountyhunters.api.Bounty;
import net.Indyuce.bountyhunters.api.CustomItem;
import net.Indyuce.bountyhunters.api.language.Language;
import net.Indyuce.bountyhunters.util.UtilityMethods;
import org.apache.commons.lang3.Validate;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class PlayerHunting {
    private final Bounty bounty;
    private final PlayerData playerData;

    @Nullable
    private Player player;
    @Nullable
    private BukkitRunnable compassRunnable;
    @Nullable
    private ItemStack compass;

    private static final long RUNNABLE_PERIOD = 7;

    public PlayerHunting(@NotNull PlayerData playerData, @NotNull Bounty bounty) {
        this.playerData = playerData;
        this.bounty = bounty;
    }

    @NotNull
    public Bounty getBounty() {
        return bounty;
    }

    @NotNull
    public OfflinePlayer getHunted() {
        return getBounty().getTarget();
    }

    public void enableCompass() {
        Validate.isTrue(!isCompassActive(), "Compass is already active");

        this.player = playerData.getPlayer();

        (compassRunnable = new BukkitRunnable() {
            int ti = 0;

            public void run() {

                // Cancel runnable if any of the conditions is missing.
                if (!check()) {
                    disableCompass();
                    return;
                }

                // Update compass display name based on distance
                if (BountyHunters.getInstance().getConfigManager().showTrackDistance) {
                    final ItemMeta meta = compass.getItemMeta();
                    meta.setDisplayName(Language.COMPASS_FORMAT.format("blocks", UtilityMethods.format(bounty.getTarget().getPlayer().getLocation().distance(player.getLocation()), true)));
                    compass.setItemMeta(meta);
                }

                final Location targetLocation = bounty.getTarget().getPlayer().getLocation();
                player.setCompassTarget(targetLocation);
            }
        }).runTaskTimer(BountyHunters.getInstance(), 0, RUNNABLE_PERIOD);
    }

    public boolean isCompassActive() {
        return compassRunnable != null;
    }

    private boolean check() {
        if (!player.isOnline() || !bounty.getTarget().isOnline() || !bounty.getTarget().getPlayer().getWorld().equals(player.getWorld()))
            return false;

        if (CustomItem.BOUNTY_COMPASS.matches(player.getInventory().getItemInMainHand())) {
            compass = player.getInventory().getItemInMainHand();
            return true;
        }

        if (CustomItem.BOUNTY_COMPASS.matches(player.getInventory().getItemInOffHand())) {
            compass = player.getInventory().getItemInOffHand();
            return true;
        }

        return false;
    }

    @NotNull
    private Location getDefaultCompassLocation() {
        Location loc = player.getBedSpawnLocation();
        if (loc != null) return loc;

        return player.getWorld().getSpawnLocation();
    }

    public void disableCompass() {
        Validate.notNull(compassRunnable, "Player is not hunting");

        // Reset compass location
        player.setCompassTarget(getDefaultCompassLocation());

        // Close runnable and collect garbage
        compassRunnable.cancel();
        compassRunnable = null;
        player = null;
        compass = null;
    }

    @Deprecated
    public void hideParticles() {
        disableCompass();
    }

    @Deprecated
    public void showParticles(Player player) {
        enableCompass();
    }
}
