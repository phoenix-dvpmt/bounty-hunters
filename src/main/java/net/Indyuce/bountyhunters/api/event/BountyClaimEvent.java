package net.Indyuce.bountyhunters.api.event;

import net.Indyuce.bountyhunters.api.Bounty;
import net.Indyuce.bountyhunters.api.language.Message;
import net.Indyuce.bountyhunters.api.player.PlayerData;
import net.Indyuce.bountyhunters.util.Alerts;
import net.Indyuce.bountyhunters.util.UtilityMethods;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.jetbrains.annotations.NotNull;

public class BountyClaimEvent extends BountyEvent {
    private final Player player;
    private final Player target;
    private final boolean headHunting;

    private static final HandlerList handlers = new HandlerList();

    public BountyClaimEvent(Bounty bounty, Player player) {
        this(bounty, player, false);
    }

    /**
     * Called whenever a player claims a bounty by killing the bounty target or
     * by giving the target's head to the bounty creator
     *
     * @param bounty      Bounty claimed
     * @param player      Player claiming the bounty
     * @param headHunting If the player is doing head hunting
     */
    public BountyClaimEvent(Bounty bounty, Player player, boolean headHunting) {
        super(bounty);

        this.player = player;
        this.target = bounty.getTarget().getPlayer();
        this.headHunting = headHunting;
    }

    /**
     * @return If headhunting is enabled i.e if the victim's head
     * was given to the bounty claimer
     */
    public boolean isHeadHunting() {
        return headHunting;
    }

    @NotNull
    public Player getTarget() {
        return target;
    }

    @NotNull
    public Player getClaimer() {
        return player;
    }

    @Deprecated
    public void sendAllert() {
        Alerts.bountyClaimed(getBounty(), player);
    }

    @NotNull
    public HandlerList getHandlers() {
        return handlers;
    }

    @NotNull
    public static HandlerList getHandlerList() {
        return handlers;
    }
}
