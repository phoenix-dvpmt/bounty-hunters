package net.Indyuce.bountyhunters.api.event;

import net.Indyuce.bountyhunters.api.Bounty;
import net.Indyuce.bountyhunters.api.language.Message;
import net.Indyuce.bountyhunters.util.Alerts;
import net.Indyuce.bountyhunters.util.UtilityMethods;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.jetbrains.annotations.NotNull;

import java.util.Objects;

public class BountyExpireEvent extends BountyEvent {
    private final BountyExpireCause cause;
    private final Player player;
    private final double amount;
    private final boolean expiring;

    private static final HandlerList handlers = new HandlerList();

    /**
     * When a bounty expires due to an admin
     *
     * @param bounty Bounty expiring
     */
    public BountyExpireEvent(Bounty bounty) {
        this(bounty, null, bounty.getReward(), BountyExpireCause.ADMIN);
    }

    /**
     * Called when a player takes away his contribution from the bounty
     *
     * @param bounty Bounty expiring
     * @param player Player taking away his contribution
     */
    public BountyExpireEvent(Bounty bounty, Player player) {
        this(bounty, player, bounty.getContribution(player), BountyExpireCause.PLAYER);
    }

    public BountyExpireEvent(Bounty bounty, Player player, double amount, BountyExpireCause cause) {
        super(bounty);

        this.player = player;
        this.amount = amount;
        this.cause = cause;
        expiring = getBounty().getReward() <= amount;
    }

    @NotNull
    public BountyExpireCause getCause() {
        return cause;
    }

    public double getAmountRemoved() {
        return amount;
    }

    /**
     * @return If the bounty should disappear after the event is called
     */
    public boolean isExpiring() {
        return expiring;
    }

    public boolean hasPlayer() {
        return player != null;
    }

    @NotNull
    public Player getPlayer() {
        return Objects.requireNonNull(player, "No player caused that event");
    }

    @Deprecated
    public void sendAllert() {
        if (isExpiring()) Alerts.bountyUnregistered(getBounty(), amount);
        else Alerts.bountyRewardDecrease(getBounty(), getPlayer(), getAmountRemoved());
    }

    @NotNull
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    public static enum BountyExpireCause {

        /**
         * When an admin uses an admin command to remove the bounty
         * or when the admin removes a player's contribution
         */
        ADMIN,

        /**
         * When the creator takes away his contribution
         * or when the bounty finally expires
         */
        PLAYER,

        /**
         * When a bounty is removed due to inactivity
         */
        INACTIVITY;
    }
}
