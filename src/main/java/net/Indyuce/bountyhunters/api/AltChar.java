package net.Indyuce.bountyhunters.api;

import org.bukkit.ChatColor;

public class AltChar {
    public static final String STAR = "★";
    public static final String DIAMOND = "◆";
    public static final String SQUARE = "█";
    public static final String LIST_DASH = "▸";

    @Deprecated
    public static String apply(String str) {
        return ChatColor.translateAlternateColorCodes('&', str
                .replace("{star}", STAR)
                .replace("{diamond}", DIAMOND));
    }
}