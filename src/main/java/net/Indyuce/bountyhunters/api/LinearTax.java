package net.Indyuce.bountyhunters.api;

import net.Indyuce.bountyhunters.util.UtilityMethods;
import org.apache.commons.lang3.Validate;
import org.bukkit.configuration.ConfigurationSection;

/**
 * Tax that is composed of a flat component and a scaling component.
 * <p>
 * Example: $100 + 3% of the amount involved
 * - flat component is 100
 * - scale component is 3%
 */
public class LinearTax {
    private final double flat, scale;
    private final double min, max;
    private final boolean hasMax, hasMin;

    public LinearTax(double flat, double scale) {
        this.flat = flat;
        this.scale = scale;
        hasMin = hasMax = false;
        min = max = 0;

        Validate.isTrue(scale >= 0 && scale <= 100, "Scale must be between 0 and 100");
    }

    public LinearTax(ConfigurationSection config) {
        Validate.notNull(config, "Config cannot be null");

        this.flat = config.getDouble("flat");
        this.scale = config.getDouble("scale");
        this.hasMin = config.contains("min");
        this.hasMax = config.contains("max");
        this.min = hasMin ? config.getDouble("min") : 0;
        this.max = hasMax ? config.getDouble("max") : 0;

        Validate.isTrue(scale >= 0 && scale <= 100, "Scale must be between 0 and 100");
    }

    @Deprecated
    public double getTax(double bountyReward) {
        return calculate(bountyReward, false);
    }

    public double calculate(double bountyReward, boolean clamp) {
        double brut = UtilityMethods.truncate(flat + scale * bountyReward / 100d, 1);
        if (hasMin && brut < min) brut = min;
        if (hasMax && brut > max) brut = max;
        if (clamp && brut > bountyReward) brut = bountyReward;
        return Math.max(0, brut);
    }
}
